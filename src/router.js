import Vue from 'vue';
import VueRouter from 'vue-router';
import Landing from "./views/Landing";
import ChatApp from "./views/ChatApp";
import Chats from "./views/Chats";
import ChatLP from "./views/ChatLP";
import Chat from "./views/Chat";

Vue.use(VueRouter);
export default new VueRouter({
	mode: 'history',
	routes: [
		{ path: "/", name: "landing", component: Landing },
		{ path: "/chats", component: ChatApp, children: [
				{ path: "", name: "chats", component: Chats },
		 		{ path: ":id", name: "chat", component: Chat }
		]}
	]
});
