export const isProduction = process.env.NODE_ENV === 'production';

export const apiUrl = "localhost:8080";
