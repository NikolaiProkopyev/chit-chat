import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import defaultChatImg from "./assets/default-chat.png";
import {apiUrl} from "./config";
import {wait} from "./utils";

Vue.use(Vuex);

const lorem = `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consectetur,
							dicta dolorem ducimus error expedita fuga in ipsum maxime nam, nisi quibusdam.
							Aspernatur, magnam nam nobis rerum sequi sunt voluptas.`;

const emptyUser = { id: null, name: ""};
let subscribed = false;

export default new Vuex.Store({
	state: {
		alertText: "",
		user: emptyUser,
		chats: [
			{
				id: 1, image: defaultChatImg, title: "Chat 1", desc: lorem,
				owner: { id: 1, name: "John" },
				users: [
					{ id: 2, name: "Alice" }
				]
			},
			{
				id: 2, image: defaultChatImg, title: "Chat 2", desc: lorem,
				owner: { id: 3, name: "Bob" },
				users: [
					{ id: 1, name: "John" },
					{ id: 2, name: "Alice" }
				]
			}
		],
		mychats: [
			{
				id: 3, image: defaultChatImg, title: "Chat 3", desc: lorem,
				owner: { id: 2, name: "Alice" },
				users: [
					{ id: 0, name: "" }
				]
			},
			{
				id: 4, image: defaultChatImg, title: "Chat 4", desc: lorem,
				owner: { id: 0, name: "" },
				users: [
					{ id: 1, name: "John" }
				]
			}
		],
		chat: {
			id: 4, image: defaultChatImg, title: "Chat 4", desc: lorem,
			owner: { id: 3, name: "Bob" },
			users: [
				{ id: 1, name: "John" }
			],
			messages: [

			]
		}
	},
	getters: {
	},
	mutations: {
		"LOGIN_USER"(state, user) {
			state.user = user
		},
		"LOGOUT_USER"(state) {
			state.user = emptyUser;
		},
		"RECEIVE_MESSAGE"(state, message) {
			state.chat.messages.push(message);
		},
		'SET_ALERT_TEXT'(state, alertText) {
			state.alertText = alertText;
		},
	},
	actions: {
		async getUser(context) {
			try {
				let user = localStorage.user;
				if (user) {
					context.commit('LOGIN_USER', user);
					return;
				}
				const response = await axios({
					url: `http://${apiUrl}/auth/user`,
					method: "GET"
				});
				user = response.data;
				if (user.id) context.commit('LOGIN_USER', user);
				else context.commit('LOGIN_USER', emptyUser);
			} catch (err) {
				context.commit('SET_ALERT_TEXT', "Произошла ошибка" );
			}
		},
		async register(context, nickname) {
			try {
				const response = await axios({
					url: `http://${apiUrl}/auth/register`,
					method: "POST",
					data: { nickname }
				});
				const user = response.data;
				context.commit('LOGIN_USER', user);
				localStorage.user = JSON.stringify(user);
			} catch (err) {
				context.commit('SET_ALERT_TEXT', "Произошла ошибка" );
			}
		},

		async subscribeChatLP(context) {
			subscribed = true;
			async function subscribe() {
				if (!subscribed) return;
				try {
					const response = await axios({
						url: `http://${apiUrl}/chatlp/${context.state.chat.id}/subscribe`,
						method: "GET",
						timeout: 180000
					});
					const message = response.data;
					context.commit('RECEIVE_MESSAGE', message);
					await subscribe();
				} catch (err) {
					context.commit('SET_ALERT_TEXT', "Произошла ошибка");
					await wait(1000);
					await subscribe();
				}
			}
			await subscribe();
		},
		unsubscribeChatLP(context) {
			subscribed = false;
		},
		async sendMessageLP(context, text) {
			try {
				const response = await axios({
					url: `http://${apiUrl}/chatlp/${context.state.chat.id}/message`,
					method: "POST",
					data: {
						user: context.state.user.id,
						text: text
					}
				});
			} catch (err) {
				context.commit('SET_ALERT_TEXT', "Произошла ошибка" );
			}
		},

		subscribeChat(context) {
			this.socket = new WebSocket(`ws://${apiUrl}/chat/${context.state.chat.id}`);
			this.socket.onmessage = (e) => {
				const message = JSON.parse(e.data);
				context.commit('RECEIVE_MESSAGE', message);
			};
		},
		sendMessage(context, text) {
			const data = {
				user: context.state.user.id,
				text: text
			}
			this.socket.send(JSON.stringify(data));
		}
	}
});
