import 'babel-polyfill';
import Vue from 'vue';
import router from './router';
import store from './store';
import App from "./App";
import Fragment from 'vue-fragment'

const app = new Vue({
	router,
	store,
	el: "#app",
	render: h => h(App)
});

Vue.use(Fragment.Plugin)
