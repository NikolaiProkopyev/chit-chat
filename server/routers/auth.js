const express = require('express');
const passport = require("passport");
const { User } = require("../models/User");

const router = express.Router();

router.get("/user", ((req, res) => {
	if (req.user) {
		res.json(req.user);
	} else {
		res.json({});
	}
}));

router.post("/register", (req, res) => {
	const user = new User({
		name: req.body.nickname
	});
	user.save().then(user => {
		res.json(user);
	});
});

router.get("/google",
	passport.authenticate('google', { scope: ['profile'] })
);

router.get("/google/callback",
	passport.authenticate('google', { failureRedirect: '/' }),
	(req, res) => {
		res.redirect("/");
	}
);

router.get("/logout", ((req, res) => {
	req.logout();
	res.redirect("/");
}));

module.exports = router;
