const express = require('express');
const events = require("events");
const { User } = require("../models/User");

const messageEventEmitter = new events.EventEmitter();

const router = express.Router();

router.route("/:id/message")
	.post((req, res) => {
		User.findById(req.body.user)
			.then(user => {
				const message = {
					text: req.body.text,
					date: new Date(Date.now()),
					user: user
				};
				messageEventEmitter.emit('message', message);
				res.send("message sent");
			});
	});

router.route("/:id/subscribe")
	.get(async (req, res) => {
		messageEventEmitter.once('message', (message) => {
			res.json(message);
		});
	});

module.exports = router;
