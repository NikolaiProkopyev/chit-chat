const express = require('express');
const { User } = require("../models/User");

let connects = [];

const router = express.Router();

router.ws('/:id', function(ws, req) {
	connects.push(ws);
	ws.on('message', function(data) {
		data = JSON.parse(data);
		User.findById(data.user)
			.then(user => {
				const message = {
					text: data.text,
					date: new Date(Date.now()),
					user: user
				};
				connects.forEach(ws => {
					ws.send(JSON.stringify(message));
				});
				console.log(message);
			});
	});
});

module.exports = router;
