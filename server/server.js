const express = require('express');
const app = express();
require('express-ws')(app);
const mongoose = require('mongoose');
const path = require("path");
const cors = require("cors");
const session  = require('express-session');
const passport = require("passport");
require("./passport")(passport);

const { sessionSecret, port, connection } = require('./config');
const authRouter = require('./routers/auth');
const chatLPRouter = require('./routers/chatlp');
const chatRouter = require('./routers/chat');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(session({
	secret: sessionSecret,
	resave: false,
	saveUninitialized: false
}));

app.use(passport.initialize({}));
app.use(passport.session({}));

app.use(express.static('./public/static'));
app.use(express.static('./public/dist'));

app.use('/auth', authRouter);
app.use('/chatlp', chatLPRouter);
app.use('/chat', chatRouter);

app.get("*", (req, res) => {
	res.sendFile(path.resolve('./public/index.html'));
});

mongoose.connect(connection, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(db => console.log('MongoDB is connected'));

const server = app.listen(port, () => console.log(`App listening on http://localhost:${port}!`));
