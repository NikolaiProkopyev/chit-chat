const GoogleStrategy = require('passport-google-oauth20').Strategy;
const { User } = require('./models/User');
const { jwtConfig, googleClient } = require("./config");

module.exports = function(passport) {
	passport.use('google', new GoogleStrategy({
			clientID: googleClient.id,
			clientSecret: googleClient.secret,
			callbackURL: "/auth/google/callback"
		},
		async(accessToken, refreshToken, profile, done) => {
			console.log(profile);
			const newUser = {
				name: profile.displayName,
				googleId: profile.id
			}
			try {
				let user = await User.findOne({googleId : profile.id});
				if (!user) {
					user = await User.create(newUser);
				}
				done(null, user);
			} catch(err) {
				console.log(err);
				done(err);
			}
		}
	));
	passport.serializeUser((user, done)=> {
		done(null, user.id);
	});
	passport.deserializeUser((id, done)=> {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});
}
