const mongoose = require('mongoose');
const { UserSchema } = require("./User");
const { MessageSchema } = require("./Message");

const ChatSchema = new mongoose.Schema({
	title: String,
	desc: String,
	image: String,
	owner: UserSchema,
	users: [UserSchema],
	messages: [MessageSchema]
}).set('toJSON', {
	virtuals: true,
	transform: (doc, ret, options) => {
		delete ret.__v;
		ret.id = ret._id.toString();
		delete ret._id;
	}});

module.exports = {
	Chat: mongoose.model('Chat', ChatSchema),
	ChatSchema
};
