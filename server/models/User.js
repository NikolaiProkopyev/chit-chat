const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	name: String,
	googleId: String
}).set('toJSON', {
	virtuals: true,
	transform: (doc, ret, options) => {
		delete ret.__v;
		ret.id = ret._id.toString();
		delete ret._id;
	}});

module.exports = {
	User: mongoose.model('User', UserSchema),
	UserSchema
};
