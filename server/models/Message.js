const mongoose = require('mongoose');
const { UserSchema } = require("./User");

const MessageSchema = new mongoose.Schema({
	text: String,
	date: Date,
	user: UserSchema
}).set('toJSON', {
	virtuals: true,
	transform: (doc, ret, options) => {
		delete ret.__v;
		ret.id = ret._id.toString();
		delete ret._id;
	}});

module.exports = {
	Message: mongoose.model('Message', MessageSchema),
	MessageSchema
};
